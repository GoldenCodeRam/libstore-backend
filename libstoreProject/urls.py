"""libstoreProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from libstoreApp.serializers.tokenSerializer import MyTokenObtainPairView

from libstoreApp.views.cartAddBookview import CartAddBookView

from libstoreApp.views.clientCreateView import ClientCreateView
from libstoreApp.views.clientDetailView import ClientDetailView
from libstoreApp.views.clientChangePasswordView import ClientChangePasswordView

from libstoreApp.views.cartCreateView import CartCreateView
from libstoreApp.views.cartDeleteView import CartDeleteView
from libstoreApp.views.cartDetailView import CartDetailView
from libstoreApp.views.cartChangeValueView import CartChangeValueView

from libstoreApp.views.bookView import BookView

from rest_framework_simplejwt.views import TokenObtainPairView

urlpatterns = [
    path('login/', MyTokenObtainPairView.as_view()),
    path('create-client/', ClientCreateView.as_view(), name='create-client'),
    path('get-client/<email>/', ClientDetailView.as_view(), name='get-client'),
    path('create-cart/<email>/', CartCreateView.as_view(), name='create-cart'),
    path('delete-cart/<email>/', CartDeleteView.as_view(), name='delete-cart'),
    path('get-cart-information/<email>/', CartDetailView.as_view(), name='get-carts'),
    path('books/', BookView.as_view(), name='get-books'),
    path('client/cart/add-book/<book_id>/', CartAddBookView.as_view(), name='add-book'), 
    path('client/cart/change-value/', CartChangeValueView.as_view(), name='change-value'),
    path('client/change-password/', ClientChangePasswordView.as_view(), name='change-password'),
]
