from django.contrib import admin

from .models.book import Book
from .models.cart import Cart
from .models.category import Category
from .models.check import Check
from .models.client import Client
from .models.favorite import Favorite

admin.site.register(Client)
admin.site.register(Category)
admin.site.register(Book)
admin.site.register(Cart)
admin.site.register(Check)
admin.site.register(Favorite)
