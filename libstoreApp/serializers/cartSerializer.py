from libstoreApp.models.cart import Cart
from rest_framework import serializers

class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = ['id', 'id_client', 'date', 'total_value']

