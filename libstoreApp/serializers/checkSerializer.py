from rest_framework import serializers

from libstoreApp.models.check import Check


class CheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Check
        fields = ['id', 'id_book', 'id_cart']
