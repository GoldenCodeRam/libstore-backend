from libstoreApp.models.favorite import Favorite
from rest_framework import serializers

class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorite
        fields = ['id', 'id_book', 'id_client']
