from rest_framework import serializers

from libstoreApp.models.category import Category

class CategorySerializer(serializers.ModelSerializer):
	class Meta:
		model = Category
		fields = ['id', 'name']