from rest_framework import serializers

from libstoreApp.models.client import Client


class ClientSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, write_only=True)

    class Meta:
        model = Client
        fields = '__all__'
