from libstoreApp.models import category
from rest_framework import generics

class categoryDetailView(generics.RetrieveAPIView):
    def get(self):
        categoria=category.objects.all()
        return categoria.values ()
