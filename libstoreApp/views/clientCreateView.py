from rest_framework import status, views
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from libstoreApp.serializers.cartSerializer import CartSerializer

from libstoreApp.serializers.clientSerializer import ClientSerializer


class ClientCreateView(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = ClientSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        tokenData = {
            "email": request.data["email"],
            "password": request.data["password"]
        }
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)

        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)
