from django.conf import settings
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend
from libstoreApp.models.book import Book
from libstoreApp.models.cart import Cart
from libstoreApp.models.client import Client
from libstoreApp.serializers.checkSerializer import CheckSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

class CartAddBookView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, book_id):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        validData = tokenBackend.decode(token, verify=False)

        client = Client.objects.get(id=validData['user_id'])
        cart = client.cart

        book = Book.objects.get(id=book_id)
        cart.total_value += book.value
        cart.save()
        cheque=CheckSerializer(data={"id_book": book_id, "id_cart": cart.id})
        if cheque.is_valid():
            cheque.save()
            return Response(cheque.data, status=status.HTTP_201_CREATED)
        return Response(cheque.errors, status= status.HTTP_400_BAD_REQUEST)
