from django.conf import settings

from rest_framework import generics, status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend

from libstoreApp.models.book import Book
from libstoreApp.serializers.bookSerializer import BookSerializer
from libstoreApp.serializers.clientSerializer import ClientSerializer


class BookDetailView(generics.RetrieveAPIView):
    # Objetos de la base de datos
    queryset = Book.objects.all()
    # Serializador de los objetos
    serializer_class = BookSerializer
    # Permisos
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        return Response(Book.objects.all().values(), status=status.HTTP_200_OK)
