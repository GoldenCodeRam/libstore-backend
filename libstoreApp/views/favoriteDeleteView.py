from libstoreApp.models import Favorite
from libstoreApp.serializers.favoriteSerializer import FavoriteSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

class DeleteFavorite(APIView):
    def delete(self,id):
        favorito=Favorite.objects.get(id=id)
        Favorite.delete()
        mensaje={'mensaje':f"Favorito eliminado"}
        return Response(mensaje, status=status.HTTP_200_OK)