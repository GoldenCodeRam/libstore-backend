from libstoreApp.serializers.favoriteSerializer import FavoriteSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

class CreateFavorite(APIView):
    def post(self,request):
        favorito=FavoriteSerializer(data=request.data)
        if favorito.is_valid():
            favorito.save()
            return Response(favorito.data, status=status.HTTP_201_CREATED)
        return Response(favorito.errors, status= status.HTTP_400_BAD_REQUEST)