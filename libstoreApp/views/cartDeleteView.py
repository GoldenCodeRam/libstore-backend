from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from libstoreApp.models.cart import Cart
from libstoreApp.models.client import Client

class CartDeleteView(APIView):
    permission_classes = (IsAuthenticated,)

    def delete(self, request, email):
        try:
            client = Client.objects.get(email=email)
            client.cart.delete()
            return Response(status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
