from django.conf import settings

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.backends import TokenBackend

from libstoreApp.models.client import Client
from libstoreApp.serializers.clientSerializer import ClientSerializer


class ClientDetailView(generics.RetrieveAPIView):
    queryset = Client.objects.all()
    lookup_field = "email"
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        validData = tokenBackend.decode(token, verify=False)

        stringResponse = {}

        stringResponse = {}

        for client in self.queryset.all():
            if validData['user_id'] == client.id and client.email == kwargs['email']:
                return super().get(request, *args, **kwargs)
            else:
                stringResponse = { 'detail': 'Unauthorized Request' }
        
        return Response(stringResponse, status=status.HTTP_403_FORBIDDEN)
