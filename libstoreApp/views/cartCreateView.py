from django.conf import settings

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework_simplejwt.backends import TokenBackend

from libstoreApp.serializers.cartSerializer import CartSerializer

class CartCreateView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CartSerializer

    def post(self, request, email):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        validData = tokenBackend.decode(token, verify=False)

        serializer = self.serializer_class(data={
            "id_client": validData['user_id'],
            "date": request.data["date"],
            "total_value": request.data["total_value"]
        })
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status= status.HTTP_400_BAD_REQUEST)
