from rest_framework.permissions import AllowAny
from libstoreApp.models import Book
from libstoreApp.serializers.bookSerializer import BookSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

class BookView(APIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (AllowAny,)

    def post(self,request):
        print(request.data)
        libro=BookSerializer(data=request.data)
        if libro.is_valid():
            libro.save()
            return Response(libro.data, status=status.HTTP_201_CREATED)
        return Response(libro.errors, status= status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        return Response(self.queryset.all().values(), status=status.HTTP_200_OK)

    def delete(self,request,id):
        libro=Book.objects.get(id=id)
        libro.delete()
        mensaje={'mensaje':f"El libro con id:{id} fue eliminado correctamente"}
        return Response(mensaje, status=status.HTTP_200_OK)
