from django.conf import settings

from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend

from libstoreApp.models.client import Client


class CartChangeValueView(APIView):
    permission_classes = (IsAuthenticated,)

    def put(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        validData = tokenBackend.decode(token, verify=False)

        client = Client.objects.get(id=validData['user_id'])
        try:
            cart = client.cart
            print(request.data["newValue"])
            return Response(status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)

