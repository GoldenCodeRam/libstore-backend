from django.conf import settings

from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend

from libstoreApp.models import Cart
from libstoreApp.models.book import Book
from libstoreApp.models.client import Client
from libstoreApp.serializers.bookSerializer import BookSerializer
from libstoreApp.serializers.cartSerializer import CartSerializer
from libstoreApp.serializers.clientSerializer import ClientSerializer


class CartDetailView(generics.RetrieveAPIView):
    serializer_class = ClientSerializer
    lookup_field = 'email'
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        validData = tokenBackend.decode(token, verify=False)

        client = Client.objects.get(id=validData['user_id'])
        try:
            books = []
            for book in Book.objects.all():
                for check in client.cart.check_set.all():
                    if book.id == check.id_book_id:
                        books.append(book)
                        
            books = [BookSerializer(book).data for book in books]
            return Response({
                "cartInformation": CartSerializer(client.cart).data,
                "items": books
            },
            status=status.HTTP_200_OK)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
