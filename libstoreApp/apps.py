from django.apps import AppConfig


class LibstoreappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'libstoreApp'
