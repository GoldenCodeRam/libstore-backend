from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

class ClientManager(BaseUserManager):
    def create_user(self, clientEmail, password=None):
        if not clientEmail:
            raise ValueError("Users must have an email")

        client = self.model(clientEmail=self.normalize_email(clientEmail))
        client.set_password(password)
        client.save(using=self._db)
        return client

    def create_superuser(self, clientEmail, password):
        client = self.create_user(
                    clientEmail=clientEmail,
                    password=password
                )
        client.is_admin = True
        client.is_staff = True
        client.save(using=self._db)
        return client


class Client(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('name', max_length=30)
    email = models.EmailField('email', max_length=255, unique=True, db_index=True)
    password = models.CharField('password', max_length=256)
    address = models.CharField('address', max_length=50)
    phone = models.IntegerField('phone')

    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = ClientManager()

    USERNAME_FIELD = 'email'

    def save(self, **kwargs):
        self.set_password(self.password)
        super().save(**kwargs)
