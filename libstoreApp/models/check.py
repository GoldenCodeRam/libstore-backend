from django.db import models

from .book import Book
from .cart import Cart


class Check(models.Model):
    class Meta:
        unique_together = (("id_book", "id_cart"),)

    id_book = models.ForeignKey(Book, on_delete=models.CASCADE)
    id_cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
