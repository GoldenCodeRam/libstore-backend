from django.db import models

from .client import Client


class Cart(models.Model):
    id = models.AutoField(primary_key=True)
    id_client = models.OneToOneField(Client, on_delete=models.CASCADE)
    date = models.DateField()
    total_value = models.FloatField()
