from django.db import models

from .client import Client
from .book import Book


class Favorite(models.Model):
    class Meta:
        unique_together = (("id_book", "id_client"),)

    id_book = models.ForeignKey(Book, on_delete=models.CASCADE)
    id_client = models.ForeignKey(Client, on_delete=models.CASCADE)
