from django.db import models

from .category import Category


class Book(models.Model):
    id = models.AutoField(primary_key=True) 
    title = models.CharField(max_length=50)
    abstract = models.CharField(max_length=300)
    category = models.CharField(max_length=100)
    value = models.FloatField()
